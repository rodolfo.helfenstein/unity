plugins {
    kotlin("multiplatform") version "1.3.20"
    id("maven-publish")
}

group = "com.helfens.unity"

repositories {
    mavenLocal()
    mavenCentral()
}

var iosArm32FrameworkPath = ""
var iosArm64FrameworkPath = ""
var iosX64FrameworkPath = ""

kotlin() {
    
    jvm() {
        mavenPublication {
            artifactId = "Unity"
        }
    }
    
    iosArm32().binaries {
        framework(listOf(DEBUG, RELEASE))
    }
    
    iosArm64().binaries {
       framework(listOf(DEBUG, RELEASE))
    }
    
    iosX64().binaries {
       framework(listOf(DEBUG, RELEASE))
    }

    sourceSets {
        val commonTest by getting {
            dependencies {
                api("org.jetbrains.kotlin:kotlin-test-junit")
                api("org.jetbrains.kotlin:kotlin-test-testng")
                api("org.jetbrains.kotlin:kotlin-test-common")
                api("org.jetbrains.kotlin:kotlin-test-annotations-common")
            }
        }
    }

}

task("unityPublish") {
    dependsOn("clean")
    dependsOn("iosPublish")
    dependsOn("androidPublish")
}

task("iosPublish") {
    dependsOn("lipo")
}

task("androidPublish") {
    dependsOn("build")
    dependsOn("publishJvmPublicationToMavenLocal")
    dependsOn("publishMetadataPublicationToMavenLocal")
}

task<Exec>("publishUniversalFrameworkToCocoaPods") {
    dependsOn("createUniversalFramework")
    args = listOf(
        "repo",
        "push",
        "${rootProject.name}",
        "${rootProject.name}.podspec"
    )
    executable = "pod"

}

task<Exec>("createFrameworks") {
    dependsOn("build")

    var outputFile = File("$buildDir/bin/iosUniversal/${rootProject.name}.framework/${rootProject.name}")
    outputs.file(outputFile)

    args = listOf(
        "$iosArm32FrameworkPath/${rootProject.name}",
        "$iosArm64FrameworkPath/${rootProject.name}",
        "$iosX64FrameworkPath/${rootProject.name}",
        "-create",
        "-output",
        "$buildDir/bin/iosUniversal/${rootProject.name}.framework/${rootProject.name}"
    )

    executable = "lipo"
}

task<Copy>("createUniversalFramework") {
    dependsOn("createFrameworks")

    from("$buildDir/bin/iosArm32/releaseFramework") {
        include("*/Headers/*")
        include("*/Modules/*")
        include("*/Info.plist")
    }
    from("$buildDir/bin/iosArm64/releaseFramework") {
        include("*/Headers/*")
        include("*/Modules/*")
        include("*/Info.plist")
    }
    from("$buildDir/bin/iosX64/releaseFramework") {
        include("*/Headers/*")
        include("*/Modules/*")
        include("*/Info.plist")
    }
    from("$buildDir/bin/iosUniversal/releaseFramework")

    into("${rootProject.rootDir}/build/dist/ios")
}